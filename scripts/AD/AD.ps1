Configuration AD
{
  Param(
    # Domain Name for the new Forest
    [Parameter(Mandatory)]
    [string] $DomainName,
    [Parameter(Mandatory)]
    [string] $DomainNetBiosName,
    [Parameter(Mandatory)]
    [System.Management.Automation.PSCredential] $AdminCredentials,
    [Parameter(Mandatory)]
    [String] $EngagementName,
    [int] $BotCount = 1,
    [int] $ControllerCount = 1,
    [int] $AppServerCount = 1
  )

  Import-DscResource -ModuleName xActiveDirectory
  
  [System.Management.Automation.PSCredential]$DomainCreds = New-Object System.Management.Automation.PSCredential ("${DomainNetBiosName}\$($AdminCredentials.UserName)", $AdminCredentials.Password)
  [System.Management.Automation.PSCredential[]]$AppAccounts = @()
  [System.Management.Automation.PSCredential[]]$ControllerAccounts = @()
  [System.Management.Automation.PSCredential[]]$BotAccounts = @()

  For($i=0;$i -lt $AppServerCount;$i++)
  {
    $AppAccounts += New-Object System.Management.Automation.PSCredential(("BP.App.{0}" -f ($i+1).ToString("00")),(ConvertTo-SecureString "/XkMm-aUFp*tAiz6#d3jD+hcf" -AsPlainText -Force))
  }

  For($i=0;$i -lt $ControllerCount;$i++)
  {
    $ControllerAccounts += New-Object System.Management.Automation.PSCredential(("BP.Controller.{0}" -f ($i+1).ToString("00")),(ConvertTo-SecureString "/XkMm-aUFp*tAiz6#d3jD+hcf" -AsPlainText -Force))
  }

  For($i=0;$i -lt $BotCount;$i++)
  {
    $BotAccounts += New-Object System.Management.Automation.PSCredential(("BP.Runtime.{0}" -f ($i+1).ToString("00")),(ConvertTo-SecureString "/XkMm-aUFp*tAiz6#d3jD+hcf" -AsPlainText -Force))
  }

  Node localhost
  {
    LocalConfigurationManager 
    {
      ConfigurationMode = 'ApplyOnly'
      RebootNodeIfNeeded = $true
    }

    WindowsFeature ADDSInstall 
    {
      Ensure = "Present"
      Name = "AD-Domain-Services"
    } 

    WindowsFeature ADAdminCenter
    {
      Ensure = "Present"
      Name = "RSAT-AD-AdminCenter"
      DependsOn = "[WindowsFeature]ADDSInstall"
    }

    WindowsFeature ADDSTools 
    {
      Ensure = "Present"
      Name = "RSAT-ADDS-Tools"
      DependsOn = "[WindowsFeature]ADDSInstall"
    }

    xADDomain ConfigureDomain
    { 
      DomainName = $DomainName
      DomainAdministratorCredential = $DomainCreds 
      SafemodeAdministratorPassword = $DomainCreds
      DomainNetBIOSName = $DomainNetBiosName.ToUpper()
      DependsOn = "[WindowsFeature]ADDSTools", "[WindowsFeature]ADAdminCenter"
    }

    xWaitForADDomain domainReady
    {
      DomainName = $DomainName
      RetryIntervalSec = 30
      RetryCount = 20
      DependsOn = "[xADDomain]ConfigureDomain"
    }

    ForEach($AppAccount In $AppAccounts)
    {
      xADUser $AppAccount.UserName
      {
        DomainName = $DomainName
        UserName = $AppAccount.UserName
        Password = $AppAccount
        UserPrincipalName = "{0}@{1}" -f $AppAccount.UserName, $DomainName
        GivenName = $AppAccount.UserName
        Surname = $EngagementName
        DependsOn = "[xWaitForADDomain]domainReady"
      }
    }

    ForEach($ControllerAccount In $ControllerAccounts)
    {
      xADUser $ControllerAccount.UserName
      {
        DomainName = $DomainName
        UserName = $ControllerAccount.UserName
        Password = $ControllerAccount
        UserPrincipalName = "{0}@{1}" -f $ControllerAccount.UserName, $DomainName
        GivenName = $ControllerAccount.UserName
        Surname = $EngagementName
        DependsOn = "[xWaitForADDomain]domainReady"
      }
    }

    ForEach($BotAccount In $BotAccounts)
    {
      xADUser $BotAccount.UserName
      {
        DomainName = $DomainName
        UserName = $BotAccount.UserName
        Password = $BotAccount
        UserPrincipalName = "{0}@{1}" -f $BotAccount.UserName, $DomainName
        GivenName = $BotAccount.UserName
        Surname = $EngagementName
        DependsOn = "[xWaitForADDomain]domainReady"
      }
    }

    xADGroup BotsGroup
    {
      GroupName = "S-U-{0}-Runtimes" -f $EngagementName
      Category = "Security"
      GroupScope = "Universal"
      Members = [String[]]($BotAccounts | % UserName)
      DependsOn = [String[]]($BotAccounts | ForEach-Object -Begin {$Result = @()} -Process {$Result += "[xADUser]{0}" -f $_.UserName} -End {$Result})
    }

    xADGroup ControllersGroup
    {
      GroupName =  "S-U-{0}-Controllers" -f $EngagementName
      Category = "Security"
      GroupScope = "Universal"
      Members = [String[]]($ControllerAccounts | % UserName)
      DependsOn = [String[]]($ControllerAccounts | ForEach-Object -Begin {$Result = @()} -Process {$Result += "[xADUser]{0}" -f $_.UserName} -End {$Result})
    }

    xADGroup ApplicationServersGroup
    {
      GroupName =  "S-U-{0}-Applications" -f $EngagementName
      Category = "Security"
      GroupScope = "Universal"
      Members = [String[]]($AppAccounts | % UserName)
      DependsOn = [String[]]($AppAccounts | ForEach-Object -Begin {$Result = @()} -Process {$Result += "[xADUser]{0}" -f $_.UserName} -End {$Result})
    }
  }
}