﻿param(
    $VirtualMachine,
    $Dns,
    $ResourceGroup
)


$connectionName = "AzureRunAsConnection"
Write-Output "[$(Get-date)] Logging in to Azure..."    
    $servicePrincipalConnection=Get-AutomationConnection -Name $connectionName     
    $account = Add-AzureRmAccount `
        -ServicePrincipal `
        -TenantId $servicePrincipalConnection.TenantId `
        -ApplicationId $servicePrincipalConnection.ApplicationId `
       -CertificateThumbprint $servicePrincipalConnection.CertificateThumbprint 

if(!$Account) { 
    Throw "Authentication issue"
    Write-Error "Authentication issue" 
}else{
    Write-Output "[$(Get-date)] Authentication Succeed"
}

Select-AzureRmSubscription -Subscription "48c1110e-fd71-4d86-ae3f-2ea0b8ec700d"

"Getting info for VM: $VirtualMachine on RG $ResourceGroup"
$VMInfo = Get-AzureRmVM -ResourceGroupName $ResourceGroup -Name $VirtualMachine
$VmNicName = ($VMInfo.NetworkProfile.NetworkInterfaces.id).Split("/")[-1]
"Nic found:  $VmNicName"

$NicInfo = Get-AzureRmNetworkInterface -ResourceGroupName $ResourceGroup -Name $VmNicName

"DNS Servers $($NicInfo.DnsSettings.DnsServers)"
"Applied DNS $($NicInfo.DnsSettings.AppliedDnsServers)"
$NicInfo.DnsSettings.DnsServers = $Dns
$NicInfo.DnsSettings.AppliedDnsServers = $Dns

"DNS to be applied $DNS"
                
Set-AzureRmNetworkInterface -NetworkInterface $NicInfo 

"CHECKING"
$VMInfo = Get-AzureRmVM -ResourceGroupName $ResourceGroup -Name $VirtualMachine
$VmNicName = ($VMInfo.NetworkProfile.NetworkInterfaces.id).Split("/")[-1]
$NicInfo = Get-AzureRmNetworkInterface -ResourceGroupName $ResourceGroup -Name $VmNicName
"DNS Servers $($NicInfo.DnsSettings.DnsServers)"
"Applied DNS $($NicInfo.DnsSettings.AppliedDnsServers)"



"Finished"