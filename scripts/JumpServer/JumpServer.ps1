Configuration JumpServer
{
  Param(
    [Parameter(Mandatory)][string] $DomainName,
    [Parameter(Mandatory)][System.Management.Automation.PSCredential] $AdminCredentials,
    [Parameter(Mandatory)]
    [string] $DomainNetBiosName,
    [string] $LocalAdministratorName,
	[string] $DNS
  )

  Import-DscResource -ModuleName xComputerManagement
  Import-DscResource -ModuleName xPSDesiredStateConfiguration

  [System.Management.Automation.PSCredential]$DomainCreds = New-Object System.Management.Automation.PSCredential ("${DomainNetBiosName}\$($AdminCredentials.UserName)", $AdminCredentials.Password)
  [System.Management.Automation.PSCredential]$LocalAdmin = New-Object System.Management.Automation.PSCredential ($LocalAdministratorName, (ConvertTo-SecureString (New-Password) -AsPlain -Force))
  $DNSServers = $Dns 
  Node localhost
  {
    LocalConfigurationManager 
    {
      ConfigurationMode = 'ApplyOnly'
      RebootNodeIfNeeded = $true
    }

    Script TurnOffFirewall
    {
      GetScript = {@{}}
      TestScript = {$false}
      SetScript = {
        netsh advfirewall set allprofiles state off
      }
    }
	
	xComputer DomainJoin
    {
      Name = $env:COMPUTERNAME
      DomainName = $DomainName
      Credential = $DomainCreds
      DependsOn = "[Script]TurnOffFirewall"
    }
    
    xUser BuiltInAdministrator
    {
      UserName = $LocalAdmin.userName
      Disabled = $true
      Password = $LocalAdmin
    }
    
    Script ForceDNSRegistration
    {
      DependsOn = "[xComputer]DomainJoin"
      GetScript = {@{}}
      TestScript = {$false}
      SetScript = {
        Start-Process -FilePath "$Env:SystemRoot\System32\ipconfig.exe" -ArgumentList "/registerdns" -Wait
      }
    }
  }
}

Function New-Password
{
  Param(
    [int]$Length = 24
  )

  $ascii = @('!','#','%','&','(',')','*','+','-','.','/','0','1','2','3','4','5','6','7','8','9',':',';','<','=','>','?','@','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','[','\',']','_','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','{','|','}')

  For($loop=1;$loop -le $Length;$loop++)
  {
    $TempPassword += ($ascii | Get-Random)
  }

  return $TempPassword
}