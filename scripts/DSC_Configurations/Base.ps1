Configuration Base
{
    Node localhost{
        Script TurnOffFirewall
        {
           GetScript = {@{}}
           TestScript = {$false}
           SetScript = {
             netsh advfirewall set allprofiles state off
            }
        }
    }
}