Configuration MSB1_0{
 
     Import-DscResource –ModuleName PSDesiredStateConfiguration
 
     Node localhost{

       LocalConfigurationManager            
       {                                   
                ConfigurationMode = 'ApplyOnly'            
                RebootNodeIfNeeded = $true
                ActionAfterReboot = 'ContinueConfiguration'       
       }

        $data ='c:\\windows\\web\\wallpaper\\windows\\img0.jpg'
        Registry HKEY_CURRENT_USER_Control_Panel_Desktop_Wallpaper
        {
            Key = 'HKEY_CURRENT_USER\Control Panel\Desktop'
            ValueName = 'Wallpaper'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'String'
        }
            $data ='1'
        Registry HKEY_CURRENT_USER_Control_Panel_Desktop_ScreenSaveActive
        {
            Key = 'HKEY_CURRENT_USER\Control Panel\Desktop'
            ValueName = 'ScreenSaveActive'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'String'
        }
            $data ='1'
        Registry HKEY_CURRENT_USER_Control_Panel_Desktop_ScreenSaverIsSecure
        {
            Key = 'HKEY_CURRENT_USER\Control Panel\Desktop'
            ValueName = 'ScreenSaverIsSecure'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'String'
        }
            $data ='600'
        Registry HKEY_CURRENT_USER_Control_Panel_Desktop_ScreenSaveTimeOut
        {
            Key = 'HKEY_CURRENT_USER\Control Panel\Desktop'
            ValueName = 'ScreenSaveTimeOut'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'String'
        }
            $data ='scrnsave.scr'
        Registry HKEY_CURRENT_USER_Control_Panel_Desktop_SCRNSAVE_EXE
        {
            Key = 'HKEY_CURRENT_USER\Control Panel\Desktop'
            ValueName = 'SCRNSAVE.EXE'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'String'
        }
        #  -- Hide User Account control panel applet to prevent using "Stored User Names and Passwords" feature.
            $data ='00000001'
        Registry HKEY_CURRENT_USER_Software_Microsoft_Windows_CurrentVersion_Policies_Explorer_DisallowCpl
        {
            Key = 'HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer'
            ValueName = 'DisallowCpl'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='@C:\\Windows\\System32\\usercpl.dll,-1'
        Registry HKEY_CURRENT_USER_Software_Microsoft_Windows_CurrentVersion_Policies_Explorer_DisallowCpl_UA
        {
            Key = 'HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer\DisallowCpl'
            ValueName = 'UA'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'String'
        }
            $data ='00000000'
        Registry HKEY_CURRENT_USER_Software_Microsoft_Windows_NT_CurrentVersion_Network_NwCategoryWizard_Show
        {
            Key = 'HKEY_CURRENT_USER\Software\Microsoft\Windows NT\CurrentVersion\Network\NwCategoryWizard'
            ValueName = 'Show'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000000'
        Registry HKEY_CURRENT_USER_Software_Microsoft_Windows_NT_CurrentVersion_Network_NwCategoryWizard_Suppress
        {
            Key = 'HKEY_CURRENT_USER\Software\Microsoft\Windows NT\CurrentVersion\Network\NwCategoryWizard'
            ValueName = 'Suppress'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_CURRENT_USER_Software_Policies_Microsoft_Internet_Explorer_Infodelivery_Restrictions_NoAddingChannels
        {
            Key = 'HKEY_CURRENT_USER\Software\Policies\Microsoft\Internet Explorer\Infodelivery\Restrictions'
            ValueName = 'NoAddingChannels'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_CURRENT_USER_Software_Policies_Microsoft_Internet_Explorer_Infodelivery_Restrictions_NoAddingSubscriptions
        {
            Key = 'HKEY_CURRENT_USER\Software\Policies\Microsoft\Internet Explorer\Infodelivery\Restrictions'
            ValueName = 'NoAddingSubscriptions'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_CURRENT_USER_Software_Policies_Microsoft_Internet_Explorer_Infodelivery_Restrictions_NoScheduledUpdates
        {
            Key = 'HKEY_CURRENT_USER\Software\Policies\Microsoft\Internet Explorer\Infodelivery\Restrictions'
            ValueName = 'NoScheduledUpdates'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='no'
        Registry HKEY_CURRENT_USER_Software_Policies_Microsoft_Internet_Explorer_Main_Use_FormSuggest
        {
            Key = 'HKEY_CURRENT_USER\Software\Policies\Microsoft\Internet Explorer\Main'
            ValueName = 'Use FormSuggest'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'String'
        }
            $data ='0c00000060000000600000003400000034000000af010000a80100000000010001000000c301000085000000d90400005d030000e80300000000000000000000000000000b0000000100000000000000c0675c86f77f0000000000000000000000000000ea0000001e0000008990000000000000ff00000001015002000000000c00000000000000e8675c86f77f00000000000000000000ffffffff960000001e0000008b90000001000000000000000010100100000000030000000000000000685c86f77f0000000000000000000001000000640000001e0000008c90000002000000000000000102120000000000040000000000000060685c86f77f00000000000000000000ffffffff960000001e0000008d90000003000000000000000001100100000000020000000000000080685c86f77f00000000000000000000ffffffff320000001e0000008a90000004000000000000000008200100000000050000000000000098685c86f77f00000000000000000000ffffffffc80000001e0000008e900000050000000000000000011001000000000600000000000000c0685c86f77f00000000000000000000ffffffff040100001e0000008f900000060000000000000000011001000000000700000000000000e8685c86f77f00000000000000000000ffffffff49000000490000009090000007000000000000000004250000000000080000000000000008695c86f77f00000000000000000000ffffffff49000000490000009190000008000000000000000004250000000000090000000000000020695c86f77f00000000000000000000ffffffff490000004900000092900000090000000000000000042508000000000a0000000000000038695c86f77f00000000000000000000ffffffff4900000049000000939000000a000000000000000004250800000000030000000a0000000100000000000000c0675c86f77f0000000000000000000000000000d7000000000000008990000000000000ff0000000101500200000000040000000000000060685c86f77f000000000000000000000100000096000000000000008d90000001000000000000000101100000000000030000000000000000685c86f77f00000000000000000000ffffffff64000000000000008c900000020000000000000000021000000000000b0000000000000058695c86f77f0000000000000000000003000000640000000000000094900000030000000000000001021000000000000c0000000000000080695c86f77f00000000000000000000ffffffff640000000000000095900000040000000000000000011001000000000d00000000000000a8695c86f77f0000000000000000000005000000320000000000000096900000050000000000000001042001000000000e00000000000000d0695c86f77f0000000000000000000006000000320000000000000097900000060000000000000001042001000000000f00000000000000f0695c86f77f0000000000000000000007000000460000000000000098900000070000000000000001011001000000001000000000000000106a5c86f77f00000000000000000000ffffffff640000000000000099900000080000000000000000011001000000000600000000000000c0685c86f77f000000000000000000000900000004010000000000008f90000009000000000000000101100100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000004000000090000000100000000000000c0675c86f77f0000000000000000000000000000d7000000000000009e90000000000000ff00000001015002000000001100000000000000386a5c86f77f00000000000000000000ffffffff2d000000000000009b900000010000000000000000042001000000001300000000000000586a5c86f77f00000000000000000000ffffffff64000000000000009d900000020000000000000000011001000000001200000000000000806a5c86f77f00000000000000000000ffffffff64000000000000009c90000003000000000000000001100100000000030000000000000000685c86f77f000000000000000000000100000064000000000000008c900000040000000000000001021000000000000700000000000000e8685c86f77f000000000000000000000700000049000000490000009090000005000000000000000104210000000000080000000000000008695c86f77f000000000000000000000800000049000000490000009190000006000000000000000104210000000000090000000000000020695c86f77f0000000000000000000009000000490000004900000092900000070000000000000001042108000000000a0000000000000038695c86f77f000000000000000000000a000000490000004900000093900000080000000000000001042108000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000002000000080000000100000000000000c0675c86f77f0000000000000000000000000000c600000000000000b090000000000000ff00000001015002000000001500000000000000a06a5c86f77f00000000000000000000ffffffff6b00000000000000b1900000010000000000000000042500000000001600000000000000d06a5c86f77f00000000000000000000ffffffff6b00000000000000b2900000020000000000000000042500000000001800000000000000f86a5c86f77f00000000000000000000ffffffff6b00000000000000b4900000030000000000000000042500000000001700000000000000206b5c86f77f00000000000000000000ffffffff6b00000000000000b3900000040000000000000000042500000000001900000000000000586b5c86f77f00000000000000000000ffffffffa000000000000000b5900000050000000000000000042001000000001a00000000000000886b5c86f77f00000000000000000000ffffffff7d00000000000000b6900000060000000000000000042001000000001b00000000000000b86b5c86f77f00000000000000000000ffffffff7d00000000000000b790000007000000000000000004200100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000da00000000000000000000000000000000000000000000005d0200801200000091000000640000003200000064000000500000003200000028000000500000003c0000005000000050000000320000005000000050000000500000005000000050000000500000002800000050000000230000002300000023000000230000005000000050000000500000003200000032000000320000007800000064000000500000003c000000500000005000000065000000320000000000000000000000000000000100000002000000210000000300000005000000040000000700000008000000060000000a0000000b0000000c0000000d0000000e0000000f000000100000001100000012000000130000001400000015000000160000001700000018000000190000001a0000001b0000001c0000001d0000001e00000009000000200000002400000022000000230000001f00000025000000000000000000000000000000000000001f000000000000006400000032000000780000005000000050000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000010000000200000003000000040000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000040000000000000001000000000000000000000000000000'
        Registry HKEY_CURRENT_USER_Software_Microsoft_Windows_CurrentVersion_TaskManager_Preferences
        {
            Key = 'HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\TaskManager'
            ValueName = 'Preferences'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Binary'
        }
            $data ='00000000'
        Registry HKEY_CURRENT_USER_SOFTWARE_Microsoft_Windows_CurrentVersion_Explorer_ShowFrequent
        {
            Key = 'HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer'
            ValueName = 'ShowFrequent'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000000'
        Registry HKEY_CURRENT_USER_SOFTWARE_Microsoft_Windows_CurrentVersion_Explorer_ShowRecent
        {
            Key = 'HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer'
            ValueName = 'ShowRecent'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_CURRENT_USER_Software_Microsoft_Windows_CurrentVersion_Explorer_Advanced_DontPrettyPath
        {
            Key = 'HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced'
            ValueName = 'DontPrettyPath'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000000'
        Registry HKEY_CURRENT_USER_Software_Microsoft_Windows_CurrentVersion_Explorer_Advanced_HideFileExt
        {
            Key = 'HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced'
            ValueName = 'HideFileExt'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000000'
        Registry HKEY_CURRENT_USER_Software_Microsoft_Windows_CurrentVersion_Explorer_Advanced_SharingWizardOn
        {
            Key = 'HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced'
            ValueName = 'SharingWizardOn'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_CURRENT_USER_Software_Microsoft_Windows_CurrentVersion_Explorer_Advanced_ShowStatusBar
        {
            Key = 'HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced'
            ValueName = 'ShowStatusBar'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_CURRENT_USER_Software_Microsoft_Windows_CurrentVersion_Explorer_Advanced_LaunchTo
        {
            Key = 'HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced'
            ValueName = 'LaunchTo'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_CURRENT_USER_Software_Microsoft_Windows_CurrentVersion_Explorer_Advanced_Hidden
        {
            Key = 'HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced'
            ValueName = 'Hidden'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000000'
        Registry HKEY_CURRENT_USER_Software_Microsoft_Windows_CurrentVersion_Explorer_Advanced_Start_SearchFiles
        {
            Key = 'HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced'
            ValueName = 'Start_SearchFiles'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000000'
        Registry HKEY_CURRENT_USER_Software_Microsoft_Windows_CurrentVersion_Explorer_Advanced_Start_TrackDocs
        {
            Key = 'HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced'
            ValueName = 'Start_TrackDocs'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000000'
        Registry HKEY_CURRENT_USER_Software_Microsoft_Windows_CurrentVersion_Explorer_Advanced_Start_TrackProgs
        {
            Key = 'HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced'
            ValueName = 'Start_TrackProgs'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_CURRENT_USER_SOFTWARE_Microsoft_Windows_CurrentVersion_Explorer_CabinetState_FullPath
        {
            Key = 'HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\CabinetState'
            ValueName = 'FullPath'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='cccccc00aeaeae0092929200767676004f4f4f003737370026262600d1343800'
        Registry HKEY_CURRENT_USER_SOFTWARE_Microsoft_Windows_CurrentVersion_Explorer_Accent_AccentPalette
        {
            Key = 'HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Accent'
            ValueName = 'AccentPalette'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Binary'
        }
            $data ='ff4f4f4f'
        Registry HKEY_CURRENT_USER_SOFTWARE_Microsoft_Windows_CurrentVersion_Explorer_Accent_StartColorMenu
        {
            Key = 'HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Accent'
            ValueName = 'StartColorMenu'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='ff767676'
        Registry HKEY_CURRENT_USER_SOFTWARE_Microsoft_Windows_CurrentVersion_Explorer_Accent_AccentColorMenu
        {
            Key = 'HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Accent'
            ValueName = 'AccentColorMenu'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_CURRENT_USER_SOFTWARE_Microsoft_Windows_DWM_Composition
        {
            Key = 'HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\DWM'
            ValueName = 'Composition'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='c4767676'
        Registry HKEY_CURRENT_USER_SOFTWARE_Microsoft_Windows_DWM_ColorizationColor
        {
            Key = 'HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\DWM'
            ValueName = 'ColorizationColor'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000059'
        Registry HKEY_CURRENT_USER_SOFTWARE_Microsoft_Windows_DWM_ColorizationColorBalance
        {
            Key = 'HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\DWM'
            ValueName = 'ColorizationColorBalance'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='c4767676'
        Registry HKEY_CURRENT_USER_SOFTWARE_Microsoft_Windows_DWM_ColorizationAfterglow
        {
            Key = 'HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\DWM'
            ValueName = 'ColorizationAfterglow'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='0000000a'
        Registry HKEY_CURRENT_USER_SOFTWARE_Microsoft_Windows_DWM_ColorizationAfterglowBalance
        {
            Key = 'HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\DWM'
            ValueName = 'ColorizationAfterglowBalance'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_CURRENT_USER_SOFTWARE_Microsoft_Windows_DWM_ColorizationBlurBalance
        {
            Key = 'HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\DWM'
            ValueName = 'ColorizationBlurBalance'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_CURRENT_USER_SOFTWARE_Microsoft_Windows_DWM_EnableWindowColorization
        {
            Key = 'HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\DWM'
            ValueName = 'EnableWindowColorization'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000000'
        Registry HKEY_CURRENT_USER_SOFTWARE_Microsoft_Windows_DWM_ColorizationGlassAttribute
        {
            Key = 'HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\DWM'
            ValueName = 'ColorizationGlassAttribute'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00767676'
        Registry HKEY_CURRENT_USER_SOFTWARE_Microsoft_Windows_DWM_AccentColor
        {
            Key = 'HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\DWM'
            ValueName = 'AccentColor'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000000'
        Registry HKEY_CURRENT_USER_SOFTWARE_Microsoft_Windows_DWM_ColorPrevalence
        {
            Key = 'HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\DWM'
            ValueName = 'ColorPrevalence'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000000'
        Registry HKEY_CURRENT_USER_SOFTWARE_Microsoft_Windows_DWM_EnableAeroPeek
        {
            Key = 'HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\DWM'
            ValueName = 'EnableAeroPeek'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000000'
        Registry HKEY_CURRENT_USER_SOFTWARE_Microsoft_Windows_DWM_AlwaysHibernateThumbnails
        {
            Key = 'HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\DWM'
            ValueName = 'AlwaysHibernateThumbnails'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000002'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Wow6432Node_Microsoft_Windows_CurrentVersion_Internet_Settings_ZoneMap_EscDomains_localhost_*
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Internet Settings\ZoneMap\EscDomains\localhost'
            ValueName = '*'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000002'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Microsoft_Windows_CurrentVersion_Internet_Settings_ZoneMap_EscDomains_localhost_*
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings\ZoneMap\EscDomains\localhost'
            ValueName = '*'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000002'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Wow6432Node_Microsoft_Windows_CurrentVersion_Internet_Settings_ZoneMap_Domains_localhost_*
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Internet Settings\ZoneMap\Domains\localhost'
            ValueName = '*'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000002'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Microsoft_Windows_CurrentVersion_Internet_Settings_ZoneMap_Domains_localhost_*
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings\ZoneMap\Domains\localhost'
            ValueName = '*'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Windows_AppCompat_DisablePropPage
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\AppCompat'
            ValueName = 'DisablePropPage'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000000'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Windows_AppCompat_AITEnable
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\AppCompat'
            ValueName = 'AITEnable'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Windows_AppCompat_DisablePCA
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\AppCompat'
            ValueName = 'DisablePCA'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Windows_AppCompat_DisableInventory
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\AppCompat'
            ValueName = 'DisableInventory'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Windows_Personalization_NoLockScreenSlideshow
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\Personalization'
            ValueName = 'NoLockScreenSlideshow'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Windows_Personalization_NoLockScreenCamera
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\Personalization'
            ValueName = 'NoLockScreenCamera'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='C:\\Windows\\Web\\Screen\\img100.jpg'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Windows_Personalization_LockScreenImage
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\Personalization'
            ValueName = 'LockScreenImage'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'String'
        }
            $data ='00000000'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Windows_Personalization_NoChangingLockScreen
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\Personalization'
            ValueName = 'NoChangingLockScreen'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Windows_PowerShell_ModuleLogging_EnableModuleLogging
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\PowerShell\ModuleLogging'
            ValueName = 'EnableModuleLogging'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='*'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Windows_PowerShell_ModuleLogging_ModuleNames_*
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\PowerShell\ModuleLogging\ModuleNames'
            ValueName = '*'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'String'
        }
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Windows_PowerShell_ScriptBlockLogging_EnableScriptBlockLogging
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\PowerShell\ScriptBlockLogging'
            ValueName = 'EnableScriptBlockLogging'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Windows_PowerShell_ScriptBlockLogging_EnableScriptBlockInvocationLogging
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\PowerShell\ScriptBlockLogging'
            ValueName = 'EnableScriptBlockInvocationLogging'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Windows_PowerShell_Transcription_EnableTranscripting
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\PowerShell\Transcription'
            ValueName = 'EnableTranscripting'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='C:\\Windows\\Logs\\PowerShellTranscription'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Windows_PowerShell_Transcription_OutputDirectory
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\PowerShell\Transcription'
            ValueName = 'OutputDirectory'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'String'
        }
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Windows_PowerShell_Transcription_EnableInvocationHeader
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\PowerShell\Transcription'
            ValueName = 'EnableInvocationHeader'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Windows_WindowsUpdate_AU_NoAutoUpdate
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU'
            ValueName = 'NoAutoUpdate'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Windows_WindowsUpdate_AU_NoAutoRebootWithLoggedOnUsers
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU'
            ValueName = 'NoAutoRebootWithLoggedOnUsers'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='000000B4'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Windows_WindowsUpdate_AU_Re-prompt_for_restart_with_scheduled_installations
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU'
            ValueName = 'Re-prompt for restart with scheduled installations'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
        #  -- OEM and Microsoft web links within the performance control panel page are not displayed. The administrative tools will not be affected. 
            $data ='00000000'
        Registry HKEY_LOCAL_MACHINE_Software_Policies_Microsoft_Windows_Control_Panel_Performance_Control_Panel_UpsellEnabled
        {
            Key = 'HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows\Control Panel\Performance Control Panel'
            ValueName = 'UpsellEnabled'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'Dword'
        }
        #  -- Removes access to the performance center control panel page. 
            $data ='00000000'
        Registry HKEY_LOCAL_MACHINE_Software_Policies_Microsoft_Windows_Control_Panel_Performance_Control_Panel_PerfCplEnabled
        {
            Key = 'HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows\Control Panel\Performance Control Panel'
            ValueName = 'PerfCplEnabled'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'Dword'
        }
        #  -- Removes access to the performance center control panel solutions to performance problems.
            $data ='00000000'
        Registry HKEY_LOCAL_MACHINE_Software_Policies_Microsoft_Windows_Control_Panel_Performance_Control_Panel_SolutionsEnabled
        {
            Key = 'HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows\Control Panel\Performance Control Panel'
            ValueName = 'SolutionsEnabled'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'Dword'
        }
        #  -- The handwriting recognition error reporting tool enables users to report errors encountered in Tablet PC Input Panel. The tool generates error reports and transmits them to Microsoft over a secure connection. Microsoft uses these error reports to improve handwriting recognition in future versions of Windows.
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_Software_Policies_Microsoft_Windows_HandwritingErrorReports_PreventHandwritingErrorReports
        {
            Key = 'HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows\HandwritingErrorReports'
            ValueName = 'PreventHandwritingErrorReports'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
        #  -- This entry appears as MSS: (DisableIPSourceRouting) IP source routing protection level (protects against packet spoofing) in the Group Policy Object Editor. IP source routing is a mechanism that allows the sender to determine the IP route that a datagram should follow through the network.
            $data ='00000002'
        Registry HKEY_LOCAL_MACHINE_System_CurrentControlSet_Services_Tcpip_Parameters_DisableIPSourceRouting
        {
            Key = 'HKEY_LOCAL_MACHINE\System\CurrentControlSet\Services\Tcpip\Parameters'
            ValueName = 'DisableIPSourceRouting'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000002'
        Registry HKEY_LOCAL_MACHINE_System_CurrentControlSet_Services_Tcpip6_Parameters_DisableIPSourceRouting
        {
            Key = 'HKEY_LOCAL_MACHINE\System\CurrentControlSet\Services\Tcpip6\Parameters'
            ValueName = 'DisableIPSourceRouting'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
        #  -- How often keep-alive packets are sent in milliseconds
            $data ='000493e0'
        Registry HKEY_LOCAL_MACHINE_SYSTEM_CurrentControlSet_Services_Tcpip_Parameters_KeepAliveTime
        {
            Key = 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Tcpip\Parameters'
            ValueName = 'KeepAliveTime'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
        #  -- This entry appears as MSS: (PerformRouterDiscovery) Allow IRDP to detect and configure Default Gateway addresses (could lead to DoS) in the Group Policy Object Editor. It enables or disables the Internet Router Discovery Protocol (IRDP). IRDP allows the computer to detect and configure default gateway addresses automatically (as described in RFC 1256) on a per-interface basis.
            $data ='00000002'
        Registry HKEY_LOCAL_MACHINE_SYSTEM_CurrentControlSet_Services_Tcpip_Parameters_PerformRouterDiscovery
        {
            Key = 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Tcpip\Parameters'
            ValueName = 'PerformRouterDiscovery'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000000'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Camera_AllowCamera
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Camera'
            ValueName = 'AllowCamera'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Windows_CloudContent_DisableWindowsConsumerFeatures
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\CloudContent'
            ValueName = 'DisableWindowsConsumerFeatures'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Windows_CloudContent_DisableSoftLanding
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\CloudContent'
            ValueName = 'DisableSoftLanding'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Microsoft_Windows_Windows_Error_Reporting_Disabled
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\Windows Error Reporting'
            ValueName = 'Disabled'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Microsoft_Windows_Windows_Error_Reporting_DontSendAdditionalData
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\Windows Error Reporting'
            ValueName = 'DontSendAdditionalData'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Microsoft_Windows_Windows_Error_Reporting_DontShowUI
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\Windows Error Reporting'
            ValueName = 'DontShowUI'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000000'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Windows_GameUX_DownloadGameInfo
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\GameUX'
            ValueName = 'DownloadGameInfo'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000000'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Windows_GameUX_ListRecentlyPlayed
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\GameUX'
            ValueName = 'ListRecentlyPlayed'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Microsoft_Internet_Explorer_Main_DisableFirstRunCustomize
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Internet Explorer\Main'
            ValueName = 'DisableFirstRunCustomize'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000000'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_WindowsInkWorkspace_AllowWindowsInkWorkspace
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\WindowsInkWorkspace'
            ValueName = 'AllowWindowsInkWorkspace'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='000000FF'
        Registry HKEY_LOCAL_MACHINE_SYSTEM_CurrentControlSet_Services_Tcpip6_Parameters_DisabledComponents
        {
            Key = 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Tcpip6\Parameters'
            ValueName = 'DisabledComponents'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Microsoft_Windows_CurrentVersion_Policies_MobilityCenter_NoMobilityCenter
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\MobilityCenter'
            ValueName = 'NoMobilityCenter'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Windows_OneDrive_DisableFileSyncNGSC
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\OneDrive'
            ValueName = 'DisableFileSyncNGSC'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Windows_OneDrive_DisableFileSync
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\OneDrive'
            ValueName = 'DisableFileSync'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000000'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Windows_NT_Security_Center_SecurityCenterInDomain
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows NT\Security Center'
            ValueName = 'SecurityCenterInDomain'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Windows_SettingSync_EnableBackupForWin8Apps
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\SettingSync'
            ValueName = 'EnableBackupForWin8Apps'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000002'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Windows_SettingSync_DisableSettingSync
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\SettingSync'
            ValueName = 'DisableSettingSync'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Windows_SettingSync_DisableSettingSyncUserOverride
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\SettingSync'
            ValueName = 'DisableSettingSyncUserOverride'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Windows_NT_SystemRestore_DisableSR
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows NT\SystemRestore'
            ValueName = 'DisableSR'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Windows_NT_SystemRestore_DisableConfig
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows NT\SystemRestore'
            ValueName = 'DisableConfig'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Microsoft_Windows_CurrentVersion_Policies_Windows_TurnOffWinCal
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\Windows'
            ValueName = 'TurnOffWinCal'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Windows_WCN_UI_DisableWcnUi
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\WCN\UI'
            ValueName = 'DisableWcnUi'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000000'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Windows_Mail_ManualLaunchAllowed
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows Mail'
            ValueName = 'ManualLaunchAllowed'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000000'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_WindowsStore_DisableStoreApps
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\WindowsStore'
            ValueName = 'DisableStoreApps'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_WindowsStore_RemoveWindowsStore
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\WindowsStore'
            ValueName = 'RemoveWindowsStore'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000000'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Microsoft_Internet_Explorer_AdvancedOptions_CRYPTO_SSLREV_DefaultValue
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Internet Explorer\AdvancedOptions\CRYPTO\SSLREV'
            ValueName = 'DefaultValue'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='no'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Microsoft_Internet_Explorer_AdvancedOptions_BROWSE_FRIENDLY_ERRORS_DefaultValue
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Internet Explorer\AdvancedOptions\BROWSE\FRIENDLY_ERRORS'
            ValueName = 'DefaultValue'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'String'
        }
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SYSTEM_CurrentControlSet_Services_Tcpip_Parameters_enablepmtudiscovery
        {
            Key = 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Tcpip\Parameters'
            ValueName = 'enablepmtudiscovery'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Microsoft_Windows_CurrentVersion_Policies_System_LocalAccountTokenFilterPolicy
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System'
            ValueName = 'LocalAccountTokenFilterPolicy'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000000'
        Registry HKEY_LOCAL_MACHINE_system_currentcontrolset_control_terminal_server_fDenyTSConnections
        {
            Key = 'HKEY_LOCAL_MACHINE\system\currentcontrolset\control\terminal server'
            ValueName = 'fDenyTSConnections'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000002'
        Registry HKEY_LOCAL_MACHINE_system_currentcontrolset_control_terminal_server_WinStations_RDP-Tcp_SecurityLayer
        {
            Key = 'HKEY_LOCAL_MACHINE\system\currentcontrolset\control\terminal server\WinStations\RDP-Tcp'
            ValueName = 'SecurityLayer'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000004'
        Registry HKEY_LOCAL_MACHINE_system_currentcontrolset_control_terminal_server_WinStations_RDP-Tcp_MinEncryptionLevel
        {
            Key = 'HKEY_LOCAL_MACHINE\system\currentcontrolset\control\terminal server\WinStations\RDP-Tcp'
            ValueName = 'MinEncryptionLevel'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000000'
        Registry HKEY_LOCAL_MACHINE_Software_Policies_Microsoft_PCHealth_HelpSvc_Headlines
        {
            Key = 'HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\PCHealth\HelpSvc'
            ValueName = 'Headlines'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'Dword'
        }
        #  -- Specifies whether the Internet Connection Wizard can connect to Microsoft to download a list of Internet Service Providers (ISPs).
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_Software_Policies_Microsoft_Windows_Internet_Connection_Wizard_ExitOnMSICW
        {
            Key = 'HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows\Internet Connection Wizard'
            ValueName = 'ExitOnMSICW'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
        #  -- Specifies whether to use the Microsoft Web service for finding an application to open a file with an unhandled file association.
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_Software_Microsoft_Windows_CurrentVersion_Policies_Explorer_NoInternetOpenWith
        {
            Key = 'HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer'
            ValueName = 'NoInternetOpenWith'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
        #  -- Specifies whether the Windows Registration Wizard connects to Microsoft.com for online registration.
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_Software_Policies_Microsoft_Windows_Registration_Wizard_Control_NoRegistration
        {
            Key = 'HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows\Registration Wizard Control'
            ValueName = 'NoRegistration'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
        #  -- Specifies whether the "Order Prints Online" task is available from Picture Tasks in Windows folders.
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_Software_Microsoft_Windows_CurrentVersion_Policies_Explorer_NoOnlinePrintsWizard
        {
            Key = 'HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer'
            ValueName = 'NoOnlinePrintsWizard'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Windows_HomeGroup_DisableHomeGroup
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\HomeGroup'
            ValueName = 'DisableHomeGroup'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Internet_Explorer_Security_ActiveX_BlockNonAdminActiveXInstall
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Internet Explorer\Security\ActiveX'
            ValueName = 'BlockNonAdminActiveXInstall'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Microsoft_Windows_CurrentVersion_Policies_Explorer_NoDisconnect
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\Explorer'
            ValueName = 'NoDisconnect'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='Open command window here - As Admin'
        Registry HKEY_CLASSES_ROOT_Directory_shell_runas_
        {
            Key = 'HKEY_CLASSES_ROOT\Directory\shell\runas'
            ValueName = ''
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'String'
        }
            $data =''
        Registry HKEY_CLASSES_ROOT_Directory_shell_runas_Extended
        {
            Key = 'HKEY_CLASSES_ROOT\Directory\shell\runas'
            ValueName = 'Extended'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'String'
        }
            $data =''
        Registry HKEY_CLASSES_ROOT_Directory_shell_runas_NoWorkingDirectory
        {
            Key = 'HKEY_CLASSES_ROOT\Directory\shell\runas'
            ValueName = 'NoWorkingDirectory'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'String'
        }
            $data ='cmd.exe /s /k pushd \"%V\'
        Registry HKEY_CLASSES_ROOT_Directory_shell_runas_command_
        {
            Key = 'HKEY_CLASSES_ROOT\Directory\shell\runas\command'
            ValueName = ''
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'String'
        }
            $data ='Open command window here - As Admin'
        Registry HKEY_CLASSES_ROOT_Directory_Background_shell_runas_
        {
            Key = 'HKEY_CLASSES_ROOT\Directory\Background\shell\runas'
            ValueName = ''
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'String'
        }
            $data =''
        Registry HKEY_CLASSES_ROOT_Directory_Background_shell_runas_Extended
        {
            Key = 'HKEY_CLASSES_ROOT\Directory\Background\shell\runas'
            ValueName = 'Extended'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'String'
        }
            $data =''
        Registry HKEY_CLASSES_ROOT_Directory_Background_shell_runas_NoWorkingDirectory
        {
            Key = 'HKEY_CLASSES_ROOT\Directory\Background\shell\runas'
            ValueName = 'NoWorkingDirectory'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'String'
        }
            $data ='cmd.exe /s /k pushd \"%V\'
        Registry HKEY_CLASSES_ROOT_Directory_Background_shell_runas_command_
        {
            Key = 'HKEY_CLASSES_ROOT\Directory\Background\shell\runas\command'
            ValueName = ''
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'String'
        }
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SYSTEM_CurrentControlSet_Control_Lsa_Kerberos_Parameters_MaxPacketSize
        {
            Key = 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Lsa\Kerberos\Parameters'
            ValueName = 'MaxPacketSize'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='0000ffff'
        Registry HKEY_LOCAL_MACHINE_SYSTEM_CurrentControlSet_Control_Lsa_Kerberos_Parameters_MaxTokenSize
        {
            Key = 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Lsa\Kerberos\Parameters'
            ValueName = 'MaxTokenSize'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='Ernst & Young'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Microsoft_Windows_CurrentVersion_OEMInformation_Manufacturer
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\OEMInformation'
            ValueName = 'Manufacturer'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'String'
        }
            $data ='C:\\Windows\\system32\\oemlogo.bmp'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Microsoft_Windows_CurrentVersion_OEMInformation_Logo
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\OEMInformation'
            ValueName = 'Logo'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'String'
        }
            $data ='PowerShell.exe -NoExit -Command \"Set-Location $env:UserProfile\'
        ##COMMENTED BY ALE
        ##Registry HKEY_LOCAL_MACHINE_SOFTWARE_Microsoft_Windows_NT_CurrentVersion_Winlogon_AlternateShells_AvailableShells_40000
        ##{
        ##    Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon\AlternateShells\AvailableShells'
        ##    ValueName = '40000'
        ##    Ensure = 'Present'
        ##    Force = $true
        ##    Hex = $false
        ##    ValueData = $data 
        ##    ValueType = 'String'
        ##}
            $data ='00000002'
        Registry HKEY_LOCAL_MACHINE_SYSTEM_CurrentControlSet_Services_RemoteRegistry_Start
        {
            Key = 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\RemoteRegistry'
            ValueName = 'Start'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000000'
        Registry HKEY_LOCAL_MACHINE_Software_Microsoft_SQMClient_Windows_CEIPEnable
        {
            Key = 'HKEY_LOCAL_MACHINE\Software\Microsoft\SQMClient\Windows'
            ValueName = 'CEIPEnable'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000002'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Wow6432Node_Microsoft_Windows_CurrentVersion_Internet_Settings_ZoneMap_EscDomains_localhost_https
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Internet Settings\ZoneMap\EscDomains\localhost'
            ValueName = 'https'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000002'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Microsoft_Windows_CurrentVersion_Internet_Settings_ZoneMap_EscDomains_localhost_https
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings\ZoneMap\EscDomains\localhost'
            ValueName = 'https'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000002'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Wow6432Node_Microsoft_Windows_CurrentVersion_Internet_Settings_ZoneMap_Domains_localhost_https
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Internet Settings\ZoneMap\Domains\localhost'
            ValueName = 'https'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000002'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Microsoft_Windows_CurrentVersion_Internet_Settings_ZoneMap_Domains_localhost_https
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings\ZoneMap\Domains\localhost'
            ValueName = 'https'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='edit'
        Registry HKEY_CLASSES_ROOT_regfile_shell_
        {
            Key = 'HKEY_CLASSES_ROOT\regfile\shell'
            ValueName = ''
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'String'
        }
            $data ='Edit'
        Registry HKEY_CLASSES_ROOT_VBSFile_Shell_
        {
            Key = 'HKEY_CLASSES_ROOT\VBSFile\Shell'
            ValueName = ''
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'String'
        }
        #  -- Disable all not required Windows Services
            $data ='00000004'
        Registry HKEY_LOCAL_MACHINE_SYSTEM_CurrentControlSet_Services_idsvc_Start
        {
            Key = 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\idsvc'
            ValueName = 'Start'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000004'
            ##REVIEW ALE
        ##Registry HKEY_LOCAL_MACHINE_SYSTEM_CurrentControlSet_Services_WinDefend_Start
        ##{
        ##    Key = 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\WinDefend'
        ##    ValueName = 'Start'
        ##    Ensure = 'Present'
        ##    Force = $true
        ##    Hex = $true
        ##    ValueData = $data 
        ##    ValueType = 'Dword'
        ##}
            $data ='00000004'
        Registry HKEY_LOCAL_MACHINE_SYSTEM_CurrentControlSet_Services_WerSvc_Start
        {
            Key = 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\WerSvc'
            ValueName = 'Start'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000004'
        Registry HKEY_LOCAL_MACHINE_SYSTEM_CurrentControlSet_Services_WMPNetworkSvc_Start
        {
            Key = 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\WMPNetworkSvc'
            ValueName = 'Start'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SYSTEM_CurrentControlSet_Services_WSearch_DelayedAutoStart
        {
            Key = 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\WSearch'
            ValueName = 'DelayedAutoStart'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000003'
        Registry HKEY_LOCAL_MACHINE_SYSTEM_CurrentControlSet_Services_wuauserv_Start
        {
            Key = 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\wuauserv'
            ValueName = 'Start'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000000'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Internet_Explorer_Privacy_EnableInPrivateBrowsing
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Internet Explorer\Privacy'
            ValueName = 'EnableInPrivateBrowsing'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Windows_CredUI_DisablePasswordReveal
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\CredUI'
            ValueName = 'DisablePasswordReveal'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000000'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Microsoft_Windows_CurrentVersion_Policies_CredUI_EnumerateAdministrators
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\CredUI'
            ValueName = 'EnumerateAdministrators'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Microsoft_Windows_CurrentVersion_Policies_System_EnableLinkedConnections
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System'
            ValueName = 'EnableLinkedConnections'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Windows_CurrentVersion_Internet_Settings_Security_HKLM_Only
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\CurrentVersion\Internet Settings'
            ValueName = 'Security_HKLM_Only'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Windows_CurrentVersion_Internet_Settings_Security_options_edit
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\CurrentVersion\Internet Settings'
            ValueName = 'Security_options_edit'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Windows_CurrentVersion_Internet_Settings_Security_zones_map_edit
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\CurrentVersion\Internet Settings'
            ValueName = 'Security_zones_map_edit'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
        # Open files based on content, not file extension
            $data ='00000003'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Microsoft_Windows_CurrentVersion_Internet_Settings_Zones_1_1601
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings\Zones\1'
            ValueName = '1601'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
        # Submit non-encrypted form data  VAL: Enabled
            $data ='00000003'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Microsoft_Windows_CurrentVersion_Internet_Settings_Zones_1_2100
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings\Zones\1'
            ValueName = '2100'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Internet_Explorer_Control_Panel_FormSuggest_Passwords
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Internet Explorer\Control Panel'
            ValueName = 'FormSuggest Passwords'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
        #  -- Prevents Internet Explorer from checking whether a new version of the browser is available.
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Internet_Explorer_Infodelivery_Restrictions_NoUpdateCheck
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Internet Explorer\Infodelivery\Restrictions'
            ValueName = 'NoUpdateCheck'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
        #  -- Specifies that programs using the Microsoft Software Distribution Channel will not notify users when they install new components. The Software Distribution Channel is a means of updating software dynamically on users' computers by using Open Software Distribution (.osd) technologies.
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Microsoft_Windows_CurrentVersion_Policies_Explorer_NoMSAppLogo5ChannelNotify
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\Explorer'
            ValueName = 'NoMSAppLogo5ChannelNotify'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
        #  -- Configures Internet Explorer to keep track of the pages viewed in the History List to 30 days.
        #  -- This setting specifies the number of days that Internet Explorer tracks views of pages in the History List. To access the Temporary Internet Files and History Settings dialog box, from the Menu bar, on the Tools menu, click Internet Options, click the General tab, and then click Settings under Browsing history. 
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_Software_Policies_Microsoft_Internet_Explorer_Control_Panel_History
        {
            Key = 'HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Internet Explorer\Control Panel'
            ValueName = 'History'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='0000001e'
        Registry HKEY_LOCAL_MACHINE_Software_Policies_Microsoft_Windows_CurrentVersion_Internet_Settings_Url_History_DaysToKeep
        {
            Key = 'HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows\CurrentVersion\Internet Settings\Url History'
            ValueName = 'DaysToKeep'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
        #  -- This policy setting allows you to manage the crash detection feature of add-on Management.
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Internet_Explorer_Restrictions_NoCrashDetection
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Internet Explorer\Restrictions'
            ValueName = 'NoCrashDetection'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='no'
        Registry HKEY_LOCAL_MACHINE_Software_Microsoft_Internet_Explorer_Main_Use_FormSuggest
        {
            Key = 'HKEY_LOCAL_MACHINE\Software\Microsoft\Internet Explorer\Main'
            ValueName = 'Use FormSuggest'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'String'
        }
            $data ='no'
        Registry HKEY_LOCAL_MACHINE_Software_Microsoft_Internet_Explorer_Main_FormSuggest_Passwords
        {
            Key = 'HKEY_LOCAL_MACHINE\Software\Microsoft\Internet Explorer\Main'
            ValueName = 'FormSuggest Passwords'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'String'
        }
            $data ='no'
        Registry HKEY_LOCAL_MACHINE_Software_Microsoft_Internet_Explorer_Main_FormSuggest_PW_Ask
        {
            Key = 'HKEY_LOCAL_MACHINE\Software\Microsoft\Internet Explorer\Main'
            ValueName = 'FormSuggest PW Ask'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'String'
        }
        #  -- Allow the process to determine a file's type by examining its bit signature. Windows Internet Explorer uses this information to determine how to render the file. The FEATURE_MIME_SNIFFING feature, when enabled, allows to be set differently for each security zone by using the URLACTION_FEATURE_MIME_SNIFFING URL action flag.
            $data ='1'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Internet_Explorer_Main_FeatureControl_FEATURE_MIME_SNIFFING_explorer_exe
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_MIME_SNIFFING'
            ValueName = 'explorer.exe'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'String'
        }
        #  -- Block file downloads that navigate to a resource, that display a file download dialog box, or that are not initiated explicitly by a user action (for example, a mouse click or key press). This feature, when enabled, can be set differently for each security zone by using the URL action flag URLACTION_AUTOMATIC_DOWNLOAD_UI. 
            $data ='1'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Internet_Explorer_Main_FeatureControl_FEATURE_RESTRICT_FILEDOWNLOAD__Reserved_
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_RESTRICT_FILEDOWNLOAD'
            ValueName = '(Reserved)'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'String'
        }
            $data ='1'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Internet_Explorer_Main_FeatureControl_FEATURE_RESTRICT_FILEDOWNLOAD_explorer_exe
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_RESTRICT_FILEDOWNLOAD'
            ValueName = 'explorer.exe'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'String'
        }
        #  -- Prevent non-user initiated navigation between a page in one zone to a page in a higher security zone. This feature, when enabled, can be set differently for each security zone by using the URL action flag URLACTION_FEATURE_ZONE_ELEVATION. 
            $data ='1'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Internet_Explorer_Main_FeatureControl_FEATURE_ZONE_ELEVATION__Reserved_
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_ZONE_ELEVATION'
            ValueName = '(Reserved)'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'String'
        }
            $data ='1'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Internet_Explorer_Main_FeatureControl_FEATURE_ZONE_ELEVATION_explorer_exe
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_ZONE_ELEVATION'
            ValueName = 'explorer.exe'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'String'
        }
            $data ='1'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Internet_Explorer_Main_FeatureControl_FEATURE_ZONE_ELEVATION_iexplore_exe
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_ZONE_ELEVATION'
            ValueName = 'iexplore.exe'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'String'
        }
        #  -- Configure the Allow software to run or install even if the signature is invalid setting to Disabled so that users cannot run unsigned ActiveX components.
            $data ='00000000'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Internet_Explorer_Download_RunInvalidSignatures
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Internet Explorer\Download'
            ValueName = 'RunInvalidSignatures'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'Dword'
        }
        #  -- Internet Explorer uses Multipurpose Internet Mail Extensions (MIME) data to determine file handling procedures for files that are received through a Web server. The Consistent MIME Handling setting determines whether Internet Explorer requires that all file type information that is provided by Web servers be consistent. For example, if the MIME type of a file is text/plain but the MIME data indicates that the file is really an executable file, Internet Explorer changes its extension to reflect this executable status. This capability helps ensure that executable code cannot masquerade as other types of data that may be trusted.
            $data ='1'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Internet_Explorer_Main_FeatureControl_FEATURE_MIME_HANDLING__Reserved_
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_MIME_HANDLING'
            ValueName = '(Reserved)'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'String'
        }
            $data ='1'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Internet_Explorer_Main_FeatureControl_FEATURE_MIME_HANDLING_explorer_exe
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_MIME_HANDLING'
            ValueName = 'explorer.exe'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'String'
        }
            $data ='1'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Internet_Explorer_Main_FeatureControl_FEATURE_MIME_HANDLING_iexplore_exe
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_MIME_HANDLING'
            ValueName = 'iexplore.exe'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'String'
        }
        #  -- Because the MK protocol is not widely used, it should be blocked wherever it is not needed.
            $data ='1'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Internet_Explorer_Main_FeatureControl_FEATURE_DISABLE_MK_PROTOCOL__Reserved_
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_DISABLE_MK_PROTOCOL'
            ValueName = '(Reserved)'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'String'
        }
            $data ='1'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Internet_Explorer_Main_FeatureControl_FEATURE_DISABLE_MK_PROTOCOL_explorer_exe
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_DISABLE_MK_PROTOCOL'
            ValueName = 'explorer.exe'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'String'
        }
            $data ='1'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Internet_Explorer_Main_FeatureControl_FEATURE_DISABLE_MK_PROTOCOL_iexplore_exe
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_DISABLE_MK_PROTOCOL'
            ValueName = 'iexplore.exe'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'String'
        }
        #  -- In certain circumstances, Web sites can initiate file download prompts without interaction from users. This technique can allow Web sites to put unauthorized files on a userâ€™s hard disk drive if they click the wrong button and accept the download.
        # 
            $data ='1'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Internet_Explorer_Main_FeatureControl_FEATURE_RESTRICT_FILEDOWNLOAD_iexplore_exe
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_RESTRICT_FILEDOWNLOAD'
            ValueName = 'iexplore.exe'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'String'
        }
        #  -- Because Internet Explorer crash report information could contain sensitive information from the computer's memory, the Turn off Crash Detection setting is configured to Enabled
        #  -- MIME sniffing is a process that examines the content of a MIME file to determine its contextâ€”whether it is a data file, an executable file, or some other type of file. This policy setting configures Internet Explorer MIME sniffing to prevent promotion of a file of one type to a more dangerous file type.
            $data ='1'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Internet_Explorer_Main_FeatureControl_FEATURE_MIME_SNIFFING__Reserved_
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_MIME_SNIFFING'
            ValueName = '(Reserved)'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'String'
        }
            $data ='1'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Internet_Explorer_Main_FeatureControl_FEATURE_MIME_SNIFFING_iexplore_exe
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_MIME_SNIFFING'
            ValueName = 'iexplore.exe'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'String'
        }
        #  -- This policy setting specifies whether the tasks Publish this file to the Web, Publish this folder to the Web, and Publish the selected items to the Web are available from File and Folder Tasks in Windows folders.
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Microsoft_Windows_CurrentVersion_Policies_Explorer_NoPublishingWizard
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\Explorer'
            ValueName = 'NoPublishingWizard'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
        #  -- This policy setting allows you to disable the client computerâ€™s ability to print over HTTP, which allows the computer to print to printers on the intranet as well as the Internet.
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_Software_Policies_Microsoft_Windows_NT_Printers_DisableHTTPPrinting
        {
            Key = 'HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows NT\Printers'
            ValueName = 'DisableHTTPPrinting'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
        #  -- This policy setting helps prevent Terminal Services clients from saving passwords on a computer.
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Windows_NT_Terminal_Services_DisablePasswordSaving
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows NT\Terminal Services'
            ValueName = 'DisablePasswordSaving'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
        #  -- This policy setting configures the RPC Runtime on an RPC server to restrict unauthenticated RPC clients from connecting to the RPC server.
        #  -- Windows is prevented from downloading providers
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_Software_Microsoft_Windows_CurrentVersion_Policies_Explorer_NoWebServices
        {
            Key = 'HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer'
            ValueName = 'NoWebServices'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
        #  -- Prevent the dial-up password from being saved
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SYSTEM_CurrentControlSet_Services_RasMan_Parameters_DisableSavePassword
        {
            Key = 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\RasMan\Parameters'
            ValueName = 'DisableSavePassword'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
        #  -- Specifies when password protection of a screen saver becomes effective. This entry specifies the delay between the appearance of a password-protected screen saver and the enforcement of the password requirement.
            $data ='00000000'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Microsoft_Windows_NT_CurrentVersion_Winlogon_ScreenSaverGracePeriod
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon'
            ValueName = 'ScreenSaverGracePeriod'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'Dword'
        }
        #  -- Controls whether passwords can be saved on this computer from Remote Desktop Connection.
        #  -- Iit directs the RPC Runtime on an RPC server to restrict unauthenticated RPC clients connecting to RPC servers running on a machine. A client will be considered an authenticated client if it uses a named pipe to communicate with the server or if it uses RPC Security. RPC Interfaces that have specifically asked to be accessible by unauthenticated clients may be exempt from this restriction, depending on the selected value for this policy.
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_Software_Policies_Microsoft_Windows_NT_Rpc_RestrictRemoteClients
        {
            Key = 'HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows NT\Rpc'
            ValueName = 'RestrictRemoteClients'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
        #  -- This setting will cause RPC Clients that need to communicate with the Endpoint Mapper Service to not authenticate. The Endpoint Mapper Service on machines running Windows NT4 (all service packs) cannot process authentication information supplied in this manner. This means that enabling this setting on a client machine will prevent that client from communicating with a Windows NT4 server using RPC if endpoint resolution is needed.
            $data ='00000000'
        Registry HKEY_LOCAL_MACHINE_Software_Policies_Microsoft_Windows_NT_Rpc_EnableAuthEpResolution
        {
            Key = 'HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows NT\Rpc'
            ValueName = 'EnableAuthEpResolution'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'Dword'
        }
        #  -- Allows you to disable Windows Messenger.
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Messenger_Client_PreventRun
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Messenger\Client'
            ValueName = 'PreventRun'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000000'
        Registry HKEY_LOCAL_MACHINE_SYSTEM_CurrentControlSet_Control_SecurityProviders_SCHANNEL_Ciphers_Triple_DES_168_Enabled
        {
            Key = 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\Triple DES 168'
            ValueName = 'Enabled'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000000'
        Registry HKEY_LOCAL_MACHINE_SYSTEM_CurrentControlSet_Control_SecurityProviders_SCHANNEL_Ciphers_Triple_DES_168/168_Enabled
        {
            Key = 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\Triple DES 168/168'
            ValueName = 'Enabled'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000000'
        Registry HKEY_LOCAL_MACHINE_SYSTEM_CurrentControlSet_Control_SecurityProviders_SCHANNEL_Protocols_Multi-Protocol_Unified_Hello_Server_Enabled
        {
            Key = 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\Multi-Protocol Unified Hello\Server'
            ValueName = 'Enabled'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SYSTEM_CurrentControlSet_Control_SecurityProviders_SCHANNEL_Protocols_Multi-Protocol_Unified_Hello_Server_DisabledByDefault
        {
            Key = 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\Multi-Protocol Unified Hello\Server'
            ValueName = 'DisabledByDefault'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000000'
        Registry HKEY_LOCAL_MACHINE_SYSTEM_CurrentControlSet_Control_SecurityProviders_SCHANNEL_Protocols_PCT_1_0_Client_Enabled
        {
            Key = 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\PCT 1.0\Client'
            ValueName = 'Enabled'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SYSTEM_CurrentControlSet_Control_SecurityProviders_SCHANNEL_Protocols_PCT_1_0_Client_DisabledByDefault
        {
            Key = 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\PCT 1.0\Client'
            ValueName = 'DisabledByDefault'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000000'
        Registry HKEY_LOCAL_MACHINE_SYSTEM_CurrentControlSet_Control_SecurityProviders_SCHANNEL_Protocols_PCT_1_0_Server_Enabled
        {
            Key = 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\PCT 1.0\Server'
            ValueName = 'Enabled'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SYSTEM_CurrentControlSet_Control_SecurityProviders_SCHANNEL_Protocols_PCT_1_0_Server_DisabledByDefault
        {
            Key = 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\PCT 1.0\Server'
            ValueName = 'DisabledByDefault'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000000'
        Registry HKEY_LOCAL_MACHINE_SYSTEM_CurrentControlSet_Control_SecurityProviders_SCHANNEL_Protocols_SSL_2_0_Client_Enabled
        {
            Key = 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\SSL 2.0\Client'
            ValueName = 'Enabled'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SYSTEM_CurrentControlSet_Control_SecurityProviders_SCHANNEL_Protocols_SSL_2_0_Client_DisabledByDefault
        {
            Key = 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\SSL 2.0\Client'
            ValueName = 'DisabledByDefault'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000000'
        Registry HKEY_LOCAL_MACHINE_SYSTEM_CurrentControlSet_Control_SecurityProviders_SCHANNEL_Protocols_SSL_2_0_Server_Enabled
        {
            Key = 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\SSL 2.0\Server'
            ValueName = 'Enabled'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SYSTEM_CurrentControlSet_Control_SecurityProviders_SCHANNEL_Protocols_SSL_2_0_Server_DisabledByDefault
        {
            Key = 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\SSL 2.0\Server'
            ValueName = 'DisabledByDefault'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000000'
        Registry HKEY_LOCAL_MACHINE_SYSTEM_CurrentControlSet_Control_SecurityProviders_SCHANNEL_Protocols_SSL_3_0_Client_Enabled
        {
            Key = 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\SSL 3.0\Client'
            ValueName = 'Enabled'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SYSTEM_CurrentControlSet_Control_SecurityProviders_SCHANNEL_Protocols_SSL_3_0_Client_DisabledByDefault
        {
            Key = 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\SSL 3.0\Client'
            ValueName = 'DisabledByDefault'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000000'
        Registry HKEY_LOCAL_MACHINE_SYSTEM_CurrentControlSet_Control_SecurityProviders_SCHANNEL_Protocols_SSL_3_0_Server_Enabled
        {
            Key = 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\SSL 3.0\Server'
            ValueName = 'Enabled'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SYSTEM_CurrentControlSet_Control_SecurityProviders_SCHANNEL_Protocols_SSL_3_0_Server_DisabledByDefault
        {
            Key = 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\SSL 3.0\Server'
            ValueName = 'DisabledByDefault'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000000'
        Registry HKEY_LOCAL_MACHINE_SYSTEM_CurrentControlSet_Control_SecurityProviders_SCHANNEL_Protocols_TLS_1_0_Client_Enabled
        {
            Key = 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\TLS 1.0\Client'
            ValueName = 'Enabled'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SYSTEM_CurrentControlSet_Control_SecurityProviders_SCHANNEL_Protocols_TLS_1_0_Client_DisabledByDefault
        {
            Key = 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\TLS 1.0\Client'
            ValueName = 'DisabledByDefault'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000000'
        Registry HKEY_LOCAL_MACHINE_SYSTEM_CurrentControlSet_Control_SecurityProviders_SCHANNEL_Protocols_TLS_1_0_Server_Enabled
        {
            Key = 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\TLS 1.0\Server'
            ValueName = 'Enabled'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SYSTEM_CurrentControlSet_Control_SecurityProviders_SCHANNEL_Protocols_TLS_1_0_Server_DisabledByDefault
        {
            Key = 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\TLS 1.0\Server'
            ValueName = 'DisabledByDefault'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000000'
        Registry HKEY_LOCAL_MACHINE_SYSTEM_CurrentControlSet_Control_SecurityProviders_SCHANNEL_Protocols_TLS_1_1_Client_Enabled
        {
            Key = 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\TLS 1.1\Client'
            ValueName = 'Enabled'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SYSTEM_CurrentControlSet_Control_SecurityProviders_SCHANNEL_Protocols_TLS_1_1_Client_DisabledByDefault
        {
            Key = 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\TLS 1.1\Client'
            ValueName = 'DisabledByDefault'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000000'
        Registry HKEY_LOCAL_MACHINE_SYSTEM_CurrentControlSet_Control_SecurityProviders_SCHANNEL_Protocols_TLS_1_1_Server_Enabled
        {
            Key = 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\TLS 1.1\Server'
            ValueName = 'Enabled'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SYSTEM_CurrentControlSet_Control_SecurityProviders_SCHANNEL_Protocols_TLS_1_1_Server_DisabledByDefault
        {
            Key = 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\TLS 1.1\Server'
            ValueName = 'DisabledByDefault'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='ffffffff'
        Registry HKEY_LOCAL_MACHINE_SYSTEM_CurrentControlSet_Control_SecurityProviders_SCHANNEL_Protocols_TLS_1_2_Client_Enabled
        {
            Key = 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\TLS 1.2\Client'
            ValueName = 'Enabled'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000000'
        Registry HKEY_LOCAL_MACHINE_SYSTEM_CurrentControlSet_Control_SecurityProviders_SCHANNEL_Protocols_TLS_1_2_Client_DisabledByDefault
        {
            Key = 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\TLS 1.2\Client'
            ValueName = 'DisabledByDefault'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='ffffffff'
        Registry HKEY_LOCAL_MACHINE_SYSTEM_CurrentControlSet_Control_SecurityProviders_SCHANNEL_Protocols_TLS_1_2_Server_Enabled
        {
            Key = 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\TLS 1.2\Server'
            ValueName = 'Enabled'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000000'
        Registry HKEY_LOCAL_MACHINE_SYSTEM_CurrentControlSet_Control_SecurityProviders_SCHANNEL_Protocols_TLS_1_2_Server_DisabledByDefault
        {
            Key = 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\TLS 1.2\Server'
            ValueName = 'DisabledByDefault'
            Ensure = 'Present'
            Force = $true
            Hex = $false
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='00000000'
        #ALE REVIEW
        ##Registry HKEY_LOCAL_MACHINE_SYSTEM_CurrentControlSet_Control_SecurityProviders_SCHANNEL_Ciphers_RC4_128/128_Enabled
        ##{
        ##    Key = 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\RC4 128/128'
        ##    ValueName = 'Enabled'
        ##    Ensure = 'Present'
        ##    Force = $true
        ##    Hex = $false
        ##    ValueData = $data 
        ##    ValueType = 'Dword'
        ##}
            $data ='00000000'
        #ALE REVIEW
        ##Registry HKEY_LOCAL_MACHINE_SYSTEM_CurrentControlSet_Control_SecurityProviders_SCHANNEL_Ciphers_RC4_40/128_Enabled
        ##{
        ##    Key = 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\RC4 40/128'
        ##    ValueName = 'Enabled'
        ##    Ensure = 'Present'
        ##    Force = $true
        ##    Hex = $false
        ##    ValueData = $data 
        ##    ValueType = 'Dword'
        ##}
            $data ='00000000'
        #ALE REVIEW
        ##Registry HKEY_LOCAL_MACHINE_SYSTEM_CurrentControlSet_Control_SecurityProviders_SCHANNEL_Ciphers_RC4_56/128_Enabled
        ##{
        ##    Key = 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\RC4 56/128'
        ##    ValueName = 'Enabled'
        ##    Ensure = 'Present'
        ##    Force = $true
        ##    Hex = $false
        ##    ValueData = $data h
        ##    ValueType = 'Dword'
        ##}
            $data ='80800000'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Microsoft_Cryptography_OID_EncodingType_0_CertDllCreateCertificateChainEngine_Config_WeakSha1ThirdPartyFlags
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Cryptography\OID\EncodingType 0\CertDllCreateCertificateChainEngine\Config'
            ValueName = 'WeakSha1ThirdPartyFlags'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
            $data ='0018df076244d101'
        #Registry HKEY_LOCAL_MACHINE_SOFTWARE_Microsoft_Cryptography_OID_EncodingType_0_CertDllCreateCertificateChainEngine_Config_WeakSha1ThirdPartyAfterTime
        #{
        #    Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Cryptography\OID\EncodingType 0\CertDllCreateCertificateChainEngine\Config'
        #    ValueName = 'WeakSha1ThirdPartyAfterTime'
        #    Ensure = 'Present'
        #    Force = $true
        #    Hex = $true
        #    ValueData = $data 
        #    ValueType = 'Binary'
        #}
            $data ='00000001'
        Registry HKEY_LOCAL_MACHINE_SOFTWARE_Policies_Microsoft_Windows_Defender_DisableAntiSpyware
        {
            Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows Defender'
            ValueName = 'DisableAntiSpyware'
            Ensure = 'Present'
            Force = $true
            Hex = $true
            ValueData = $data 
            ValueType = 'Dword'
        }
    }
}